## README.md onde o trainne irá continuamente responder as perguntas em formas de commit. 

![Gif](https://c.tenor.com/YJVujQ8qJQgAAAAC/hora-de-aprender-profesor-utonio.gif)


__Respostas:__

3.  O código realiza uma soma de dois algorismos utilizando a propriedade nativa do node process.argv que contem tudo que está presente na linha de comando e assim torna-se possível utilizar da linha de comando para enviar informações para o código. A função que segue a propriedade .slice(2) faz com que os dois primeiros elementos da propriedade process.argv sejam subtraídas de uma cópia dessa, fazendo com que as partes referentes a execução do node e do caminho do código sejam ignoradas, no nosso caso, faz com que sobre apenas os dois números dentro da nova constante (args) onde a propriedade cortada é atribuida. Através disso temos a última parte do código se refere ao console.log que faz a soma dos dois números seja executada, a função parseInt transforma o valor em string e retorna o primeiro inteiro, enquanto args[0] e args[1] selecionam respectivamente o primeiro e segundo elementos do array. No final das contas temos uma soma de dois inteiros.

4.  Que tipo de commit esse código deve ter de acordo ao conventional commit. Deve ser um commit do tipo feat por adicionar uma nova funcionalidade _git commit -m "feat: função calculadora adicionada"_. Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Para atualização doREADME utilizamos o tipo docs, para atualizar a documentação _git commit -m "docs: README adicionado"_

5. Com o novo código temos o mesmo resultado com uma abordagem diferente por isso temos um commit do tipo refactor. *Referência*: https://theodorusclarence.com/library/conventional-commit-readme

10. O código implementado por João permite uma maior flexibilidade, pois não depende mais de uma função pré estabelecida dentro do código. O seu código faz com que seja possível realizar todas as funções de uma calculadora convencional, uma vez que a operação é determinada pelo usuário e qual símbolo ele utiliza na linha de comando. Ainda explicado o código temos os elementos do array sendo concatenados de forma que os números passem pela operação. Temos ainda um if que só exibira a mensagem se a função evaluate tiver um resultado.